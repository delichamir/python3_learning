#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import inout as io

best = []

while True:
    
    sp = io.input_one()
    if sp is None:
        break
    best.append(sp)
    best.sort()
    
    if len(best) > 5:
        best.pop()
        
    io.output_all(best) 
    
    
    



