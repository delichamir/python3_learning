#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def output_all(people):
    for r, s in people:
        print(f'{r:5.1f} - {s}')
