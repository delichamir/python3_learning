#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def input_one():
    surname = input('Please enter the surname of sportsman: ')
    if surname == 'k':
        return None
    result = float(input('Please enter the result of sportsman: '))
    return (result, surname)
