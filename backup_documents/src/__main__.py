#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
logging.basicConfig(
    level = logging.WARNING,
    format = '%(levelname)-8s [%(asctime)s] (%(name)s) %(message)s'
    )

from Nakladnaya import Nakladnaya as N
from datetime import datetime 

from  pos import PosPrice, PosSumma
 
print('\n')

n = N(number = 25)
n.address = "'На дорогу в деревню'"
n.created = datetime(2019, 9, 1)

n.add_pos(PosPrice, title= "Pora", quantity= 25, unit='шт', price=101)
n.add_pos(PosSumma, title= 'Копыта', unit= 'кг', summa=12)
n.add_pos(PosPrice, title= 'Лажа', quantity= 15, unit= 'тон', price = 10.15)
n.add_pos(title= 'Хвост', quantity= 1, unit= 'шт', summa=250)
n.add_pos(title= 'Пузырка крапивчатая', quantity= 12, unit= 'г', price=105.9)




n.out()
print('\n', n)


