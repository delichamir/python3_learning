#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .Position import Position


class PosSumma(Position):
    
    def __init__(self, title, quantity=1, unit=None, summa=None):
        super().__init__(title, quantity, unit)
        self.__summa = summa
    
    @property
    def summa(self):
        return self.__summa
    
    @summa.setter
    def summa(self, new_summa):
        self.__summa = new_summa
        
    @property
    def price(self):
        return self.summa / self.quantity
    
   
