#!/usr/bin/env python3
# -*- coding: utf-8 -*-



import matplotlib.pyplot as plt
import numpy as np
import sqlite3 as sql 

from pathlib import Path
from urllib.request import urlopen
from course import data_lines, data_from

url = 'https://www.cbr.ru/currency_base/dynamics/?UniDbQuery.Posted=True&UniDbQuery.mode=1&UniDbQuery.date_req1=&UniDbQuery.date_req2=&UniDbQuery.VAL_NM_RQ=R01700&UniDbQuery.FromDate=01.01.2019&UniDbQuery.ToDate=29.08.2019'

data_dir = ( Path.cwd() / 'data' ).absolute()

seq = data_lines(url)
seq = list(data_from(seq))
data = np.asarray(seq)



#x = data[:,0]
#y = data[:,2]
#plt.plot(x,y)
#plt.show()

conn = sql.connect('currency.dat')
cursor = conn.cursor()

cursor.execute('''
    create table currency(
        id integer not null primary key autoincrement,
        adate date,
        ascale integer,
        acourse numeric(10,4) 
        ) ;
''' )

for x in seq:
    cursor.execute('''
        insert into currency (adate, ascale, acourse) values (?, ?, ?) ;
        ''', x)
    
conn.commit()
conn.close()



