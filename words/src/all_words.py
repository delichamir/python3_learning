#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def all_words(path, *, encoding='utf-8'):
    with open(str(path),encoding=encoding) as txt:
        for line in txt:
            yield from line.split()
