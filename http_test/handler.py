#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from http.server import BaseHTTPRequestHandler


class Handler(BaseHTTPRequestHandler):
    
    def do_GET(self):
        print(self.path)
        self.send_response(200, 'OK')
        self.end_headers()
        x = '<html><body><p>Hello, world</p></body></html>'.encode('utf-8')
        self.wfile.write(x)
