#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from handler import Handler
from http.server import HTTPServer
from threading import Thread 



server_address = ('', 8081)
srv = HTTPServer(server_address, Handler)

srv_thread = Thread(None, target=srv.serve_forever)
srv_thread.start()



input('Press ENTER to finish ...')
print('Stopping server ...')
srv.shutdown()
srv_thread.join()
print('Goodbye ...')




