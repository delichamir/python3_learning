#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class DbConnect(object):
    
    def __init__(self):
        print('Connnecting to DataBase')
        self.__connected = True
        
    def __del__(self):
        if self.__connected:
            self.close()
        
    def close(self):
        print('Disconnectig from DataBase')
    
    def __enter__(self):
            result = DbCursor()
            print('START TRANSACTION')
            return result
        
    def __exit__(self, exctype, excvalue, traceback):
        if exctype is None:
            print('COMMIT TRANSACTION')
        else:
            print('ROLLBACK TRANSACTION')
        
        
class DbCursor(object):
    
    def execute(self, command):
        pass
        


conn = DbConnect()

with conn as cursor:
    pass

print('\n')
with conn as cursor:
    raise IndexError()
