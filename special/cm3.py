#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sqlite3 as sql 
from contextlib import contextmanager
import sys 



@contextmanager
def transaction(connection):
    cursor = connection.cursor()
    try:
        yield cursor
    
    finally:
        exctype,_,_ = sys.exc_info()
        if exctype is None:
            connection.commit()
        else: 
            connection.rolback()


conn =  sql.connect('data.sqlite3'):
    
with transaction(conn) as curs1:
    print('Working')
