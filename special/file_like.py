#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class ByteCounter(object):
    def __init__(self):
        self.__bytes_written = 0
        
    @property 
    def bytes_written(self):
        return self.__bytes_written
        
        
    def write(self, bytestr):
        self.__bytes_written += len(bytestr)
        
        
    def close(self):
        self.__bytes_written = 0 


s = ByteCounter()
try: 
    print('Hello, world!', file=s)
    print(s.bytes_written)
    print('Добрый вечер', file=s)
    print(s.bytes_written)
    
finally:
    s.close()
    
print(s.bytes_written)
