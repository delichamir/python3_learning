#!/usr/bin/env python3
# -*- coding: utf-8 -*-



class Generator(object):
    def __init__(self, a, b, *, count=None):
        self.__a = a
        self.__b = b
        self.__count = count
    
    def __iter__(self):
        return Iterator(self.__a, self.__b, self.__count)
        
        
class Iterator(object):
    
    def __init__(self, a , b, count):
        self.__a = a
        self.__b = b
        self.__count = count
        
    def __next__(self):
        if self.__count is not None:
            if self.__count <=0:
                raise StopIteration()
        
        self.__count -= 1
        result = self.__a
        x = self.__a + self.__b
        self.__a = self.__b
        self.__b = x
        return result
    
    def __iter__(self):
        return self
    

for k in Generator(2,3,count=15):
    print(k)
    
for s in Generator('a','b', count=10):
    print(s)
