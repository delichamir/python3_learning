#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Board(object):
    def __init__(self, w ,h):
        self.__w = w
        self.__h = h
    
    
    def __getitem__(self, index):
        x, y = index
        if x<0:
            x+= self.__w
        if y <0:
            y += self.__h
        if x < 0 or y < 0:
            raise IndexError()
        if x >= self.__w or y>= self.__h:
            raise IndexError()
        if (x+y) % 2 == 0:
            return 'black'
        else:
            return 'white'
            
    def __len__(self):
        return self.__w * self.__h
    
b = Board(8,8)
print('===>', len(b))
print(b[0,0])
print(b[1,-4])
print(b[-1,-1])
print(b[10,0])
        
        
