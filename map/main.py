#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import matplotlib.pyplot as plt
import numpy as np

from xml.dom.minidom import parse
from dataclasses import dataclass
from collections.abc import Sequence



@dataclass
class Point(object):
    '''
    Данные 
    '''
    iid: int
    lon : float
    lat : float 


class Points(Sequence):
    '''
    Последовательность точек 
    '''
    def __init__(self):
        self.__points = []
        self.__ids = dict()
        
    def __getitem__(self, index):
        return self.__points[index]
    
    def __len__(self):
        return len(self.__points)
    
    def append(self, pnt):
        self.__points.append(pnt)
        self.__ids[pnt.iid] = pnt
    
    def append_xml(self, element):
        iid = int(element.getAttribute('id'))
        lon = float(element.getAttribute('lon'))
        lat = float(element.getAttribute('lat'))
        p = Point(iid=iid, lon=lon, lat=lat)
        self.append(p)
        
    def by_iid(self, iid):
        return self.__ids[iid]
    
    @property
    def as_array(self):
        L = [ (x.lon, x.lat) for x in self ]
        return np.asarray(L)
    
    

class Way(Points):
    
    def __init__(self, element, ap):
        super().__init__()
        self._iid = int(element.getAttribute('id'))
        for n in element.getElementsByTagName('nd'):
            point_id = int(n.getAttribute('ref'))
            point = ap.by_iid(point_id)
            self.append(point)
    
    @property
    def iid(self):
        return self.__iid
        

    

data = parse('map.osm')
#print(data.documentElement.tagName)

#data.getElementsByTagName('node')
#data.documentElement.getElementsByTagName('node')

all_points = Points()

for n in data.getElementsByTagName('node'):
    all_points.append_xml(n)
    
    
for w in data.getElementsByTagName('way'):
    way = Way(w, all_points)
    n = way.as_array
    plt.plot(n[:,0] , n[:, 1])


plt.show()



