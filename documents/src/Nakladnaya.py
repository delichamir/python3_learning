#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from datetime import datetime 
from contextlib import suppress
from itertools import count

import logging
import pos 


LOG = logging.getLogger('docs.nakl')
LOG.setLevel(logging.INFO)

class InsufficientData(Exception): pass  # создали свой тип исключения 

class Nakladnaya(object):
    
    __counter = count(1)
    
    def __init__(self, number = None):
        
        '''
        number -- номер накладной. Если None, то номер создается автоматически
        
        '''
        self.__created = datetime.now()
        self.__positions = []
        if number is not None:
            self.__number = number
        else:
            self.__number = self.get_next_number()
    
    
    @classmethod
    def set_first_free_number(cls, number):
       cls.__counter = count(number)
       
        
    @classmethod
    def get_next_number(cls):
        return next(cls.__counter)
    
    
    @property
    def pos_count(self):
        return len(self.__positions)
    
    
    @property
    def number(self):
        return self.__number
        
    @property
    def created(self):
        return self.__created
        
    @created.setter
    def created(self, new_date):
        # TO DO: Здесь проверить можно ли менять дату 
        LOG.info(f'Дата изменена с {self.__created} на {new_date}')
        self.__created = new_date 
                
    @property        
    def address(self):
        try:
            return self.__address 
        except AttributeError as exc:
            raise InsufficientData('No address specified') from exc
        
    @address.setter
    def address(self, new_addr):
        try:
            addr = self.__address
        except AttributeError:
            addr = "'....'"
        LOG.info(f'Адрес изменен с {addr} на {new_addr}')
        self.__address = new_addr
        
    @address.deleter
    def address(self):
        LOG.info(f"Адрес '{self.__address}' удален !")
        del self.__address
        
    @property
    def positions(self):
        return iter(self.__positions)
    
    def pos(self, index):
        return self.__positions[index] 
        
        
    def __str__(self):
        no = self.number
        try:
            addr = self.address[:10]
        except InsufficientData:
            addr = '...'
        crt = self.created
        npos = self.pos_count
        return f'<Nakladnaya> #{no} [{crt}] {addr}: {npos} positions>'
    
    __repr__ = __str__
    
    def out(self):
        print('='*60)
        print(f'\t\t\tНакладная № {self.number}')
        print(f'от {self.created}')
        
        with suppress(InsufficientData):
            print('адресс: ',self.address)
            
        #try:
        #    print(self.address)
        #except InsufficientData:
        #    pass
        
        print('-'*60)
        for p in self.positions:
            p.out()
        print('='*60)
        
        
        
    def add_pos(self, postype = None, **kwargs):
        if postype is None: 
            postype = pos.guess(**kwargs)
        
        p = postype(**kwargs)
        self.__positions.append(p)
        
        
        
        
        #Создание нескольких накладных модуль shelve 
        
        # (отправитель)
    
        # отправил в рейс  (сохранить в файл, отправить контрагенту, оформит )
        # сдал
        # принял
        
        
        # позиции по накладной
        
    
        
   
    
