#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# Solving quadratic equation of  ax2+bx+c=0

a = input('Enter the number a: ')
b = input('Enter the number b: ')
c = input('Enter the number c: ')

a = float(a)
b = float(b)
c = float(c)


d = b**2-4.0*a*c
x1 = (-b - d**(1/2)) / 2.0*a
x2 = (-b + d**(1/2)) / 2.0*a

if d >=  0.0: 
    print('\nSolution is: x1={}, x2= {}'.format(x1,x2))
else: 
    print('\nThere is no solution in this program!')
    
print('Goodbye')
    

    
