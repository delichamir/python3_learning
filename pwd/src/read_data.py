#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def non_empty_lines(path):
    with path.open('rt',  encoding='utf-8') as src:
        for line in src:
            if line.strip():
                yield line
                

def login_pass(sequence):
    for x in sequence:
        login, password = x.split(';')
        r = (login.strip(), password.strip())
        yield r


