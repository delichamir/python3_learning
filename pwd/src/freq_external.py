#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import shelve
from read_data import non_empty_lines, login_pass
from itertools import count





def freq2(storage_path, data_path = None, *, first_line = 0, cnt = None ):
    
    path = str(storage_path)
    with shelve.open(path) as storage:
        
        if data_path is not None:
            seq = non_empty_lines(data_path)
            seq = login_pass(seq)
            
            if cnt is None:
                numbers = count(0)
            else:
                numbers = range(0, first_line + cnt)
                
            for k, (_, pwd) in enumerate(seq):
                print(k, pwd)
                if k < first_line:
                    continue 
                try:
                    n = storage[pwd]
                    storage[pwd] = n + 1 
                except KeyError:
                    storage[pwd] = 1
            else:
                storage.sync()
        
        
        result = []
        for k, x in enumerate(storage.items()):
            print(x)
            result.append(x)
            result.sort(key = lambda x: x[1], reverse = True)
            if len(result) > 10:
                del result[-1]
            
        
    return result 
