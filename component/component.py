#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import weakref

class Container(object):
    
    def __init__(self):
        self.name = name
        self.__left = None
        self.__right = None
    
    @property
    def left(self):
        return.__left
    
    @left.setter
    def left(self, new_component)
        self.__left = new_component
        self.__left._set_parent(self)
        
    @left.deleter
    def left(self):
        self,__left = None
        self.__left._set_parent(Nonne)
    
    def __str__(self):
        return f'<Container: {self.name}>'
        

class Component(object):
    
    def __init__(self, name):
        self.name = name 
        
    @property
    def parent(self):
        if self.__parent is None:
            return None
        else:
            return self.__parent()
        
    def _set_parent(self, parent):
        if parent is None:
            self.__parent = None
        else:
            self.__parent = weakref.ref(parent)
    
    def __str__(self):
        return f'<Component: {self.name}>'


#############################################


cont = Container()

x1 = Component('x1')
cont.left = x1

print(cont.left)

del cont.left

cont.left = Component('x2')

print(comp.left)
