#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging as log
from datetime import datetime as d
from time import sleep

log.basicConfig(level = log.DEBUG, format = '%(levelname)s %(message)s', filename = 'mylog.log')

#log.debug('This is debug message')
#log.info('This is info message')
#log.warning('Be carefull')
#log.error("Error message")
#log.critical('Fatal error')


def profile(function):
    def profiled_func(*args, **kwargs):
        t0 = d.now()
        result = function(*args, **kwargs)
        t = ( d.now() - t0 ).total_seconds()
        n = function.__name__ 
        log.debug(f'Function {n} executed {t}s')
        return result
    return profiled_func


@profile
def func1(x,y):
    z = x + y
    sleep(2.0)
    return z

s = func1(23,15)
s2 = func1(198, 1)
print(s, s2)






